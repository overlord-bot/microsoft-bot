﻿# OverlordBot.TestApp

A very simple app that serves two purposes. 

1. It allows us to call the core logic easily and see the output, this serves as a very simple manual UAT.
2. It makes sure that we keep the interface to the Core generic by having multiple callers to the Core.