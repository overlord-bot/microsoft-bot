﻿// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OverlordBot.Core.Intents;
using OverlordBot.Core.Recognizers;
using OverlordBot.Core.Repository;
using System;
using System.IO;
using System.Threading.Tasks;

namespace OverlordBot.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        private static async Task MainAsync(string[] args)
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(args, serviceCollection);
            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            var caller = serviceProvider.GetService<OverlordBotCaller>();
            await caller.Run();
        }

        private static void ConfigureServices(string[] args, IServiceCollection services)
        {

            services.AddTransient<IConfiguration>(sp =>
            {
                return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: false)
                .AddUserSecrets<Program>()
                .AddCommandLine(args)
                .AddEnvironmentVariables()
                .Build();
            });

            // You can never have enough logging
            services.AddLogging();

            // Register LUIS recognizer
            services.AddSingleton<AwacsCallRecognizer>();

            // Add the GameStateRepository so we can query the Tacview Database
            services.AddSingleton<GameStateRepository>();

            // Add all the intent processors
            services.AddSingleton<BogeyDopeIntent>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            services.AddTransient<Core.OverlordBot>();

            // Create our App that calls overlordbot with various inputs
            services.AddTransient<OverlordBotCaller>();
        }
    }
}
