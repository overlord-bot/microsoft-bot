﻿# OverlordBot.Core

Core library that contains all the core business logic for Overlord bot. Has a very simple interface
designed to be easy to call from a variety of callers.