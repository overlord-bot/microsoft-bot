// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using OverlordBot.Core.CognitiveModels;
using OverlordBot.Core.Intents;
using OverlordBot.Core.Recognizers;
using OverlordBot.Core.Repository;

namespace OverlordBot.Core
{
    public class OverlordBot
    {
        protected readonly ILogger Logger;
        private readonly AwacsCallRecognizer _awacsCallRecognizer;
        private readonly GameStateRepository _gamestate;
        private readonly BogeyDopeIntent _bogeyDope;

        public OverlordBot(ILogger<OverlordBot> logger, GameStateRepository gameState, AwacsCallRecognizer awacsCallRecognizer, BogeyDopeIntent bogeyDope) {
            Logger = logger;
            _awacsCallRecognizer = awacsCallRecognizer;
            _gamestate = gameState;
            _bogeyDope = bogeyDope;
        }

        public async Task<StringBuilder> ProcessAsync(string inputText)
        {
            var replyBuilder = new StringBuilder();

            if (!_awacsCallRecognizer.IsConfigured)
            {
                Logger.LogError("LUIS is not configured. To enable all capabilities, add 'LuisAppId', 'LuisAPIKey' and 'LuisAPIHostName' to the appsettings.json file.");
                return replyBuilder.Append("Last transmitter, our equipment is suffering startup issues.");
            }

            var luisResult = await _awacsCallRecognizer.RecognizeAsync(inputText);

            switch (luisResult.TopIntent())
            {
                case AwacsCall.Intent.radioCheck:
                    if (await ValidateCallFormatAsync(luisResult, replyBuilder))
                    {
                        replyBuilder.Append($"five-by-five");
                    }
                    break;
                case AwacsCall.Intent.bogeyDope:
                    if (await ValidateCallFormatAsync(luisResult, replyBuilder) &&
                        await ValidatePlayerInGame(luisResult, replyBuilder))
                    {
                        replyBuilder.Append(await _bogeyDope.Process(luisResult));
                    }
                    break;
                case AwacsCall.Intent.locationOfEntity:
                    if (await ValidateCallFormatAsync(luisResult, replyBuilder) &&
                        await ValidatePlayerInGame(luisResult, replyBuilder))
                    {
                        replyBuilder.Append("Location of entity");
                    }
                    break;
                case AwacsCall.Intent.setWarningRadius:
                    if (await ValidateCallFormatAsync(luisResult, replyBuilder) &&
                        await ValidatePlayerInGame(luisResult, replyBuilder))
                    {
                        replyBuilder.Append("Set warning radius");
                    }
                    break;
                case AwacsCall.Intent.picture:
                    if (await ValidateCallFormatAsync(luisResult, replyBuilder))
                    {
                        replyBuilder.Append("we do not provide picture calls");
                    }
                    break;
                case AwacsCall.Intent.None:
                    break;
                default:
                    Logger.LogError($"Unknown Intent: {luisResult.TopIntent().ToString()}");
                    return null;
            }

            return replyBuilder;
        }

        private async Task<bool> ValidateCallFormatAsync(AwacsCall luisResult, StringBuilder replyBuilder)
        {
            (string group, int? flight, int? plane) = luisResult.PlayerCallsign(AwacsCall.Role.caller);
            string awacsCallsign = luisResult.AwacsCallsign();

            if (group == null || flight == null || plane == null)
            {
                if (awacsCallsign == null)
                {
                    replyBuilder.Append("Last transmitter, AWACS could not recognise your callsign.");
                } else
                {
                    replyBuilder.Append($"Last transmitter, {awacsCallsign}, I could not recognise your callsign.");
                }
                return false;
            }
            else if(awacsCallsign == null)
            {
                replyBuilder.Append($"{group} {flight} {plane}, I could not tell if that was addressed to AWACS");
                return false;
            }
            else
            {
                replyBuilder.Append($"{group} {flight} {plane}, {awacsCallsign}, ");
            }
            return true;
        }

        private async Task<bool> ValidatePlayerInGame(AwacsCall luisResult, StringBuilder replyBuilder)
        {
            (string group, int? flight, int? plane) = luisResult.PlayerCallsign(AwacsCall.Role.caller);

            if (await _gamestate.DoesPilotExistAsync(group, (int)flight, (int)plane) == false)
            {
                replyBuilder.Append($"I cannot find you on scope.");
                return false;
            }
            return true;
        }
    }
}
