﻿// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Extensions.Logging;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace OverlordBot.Core.Repository
{
    public partial class GameStateRepository
    {
        private static readonly int CAUCUSES_MAP_MAGNETIC_OFFSET = 6;

        public async Task<Dictionary<string, int?>> BogeyDopeAsync(string group, int flight, int plane)
        {
            if (Database.State != System.Data.ConnectionState.Open)
            {
                await Database.OpenAsync();
            }
            DbDataReader dbDataReader;

            var command = @"SELECT degrees(ST_AZIMUTH(request.position, bogey.position)) as bearing,
                                      ST_DISTANCE(request.position, bogey.position) as distance,
                                      bogey.altitude, bogey.heading, bogey.pilot, bogey.group
            FROM public.units AS bogey CROSS JOIN LATERAL
              (SELECT requester.position, requester.coalition
                FROM public.units AS requester
                WHERE (requester.pilot ILIKE '" + $"%{group} {flight}-{plane}%" + @"' OR requester.pilot ILIKE '" + $"%{group} {flight}{plane}%" + @"' )
              ) as request
            WHERE NOT bogey.coalition = request.coalition
            AND bogey.type LIKE 'Air+%'
            ORDER BY request.position<-> bogey.position ASC
            LIMIT 1";

            Logger.Log(LogLevel.Debug , command);

            Dictionary<string, int?> output = null;

            using (var cmd = new NpgsqlCommand(command, Database))
            {
                dbDataReader = await cmd.ExecuteReaderAsync();
                await dbDataReader.ReadAsync();

                if (dbDataReader.HasRows)
                {
                    Logger.Log(LogLevel.Debug, $"{dbDataReader[0]}, {dbDataReader[1]}, {dbDataReader[2]}, {dbDataReader[3]}, {dbDataReader[4]}, {dbDataReader[5]}");

                    var bearing = (int)Math.Round(dbDataReader.GetDouble(0));
                    if (bearing < 0) { bearing += 360; } // Westward is expressed as negative numbers so convert positive


                    var range = (int)Math.Round((dbDataReader.GetDouble(1) * 0.539957d) / 1000); // Nautical Miles
                    var altitude = (int)Math.Round((dbDataReader.GetDouble(2) * 3.28d) / 1000d, 0) * 1000; // Feet
                    var heading = (int)dbDataReader.GetDouble(3);

                    output = new Dictionary<string, int?>
                    {
                        { "bearing", bearing - CAUCUSES_MAP_MAGNETIC_OFFSET },
                        { "range", range },
                        { "altitude", altitude },
                        { "heading", heading - CAUCUSES_MAP_MAGNETIC_OFFSET }
                    };
                }
                dbDataReader.Close();
            }

            return output;
        }
    }
}
